import Fiber from "fibers";
import Future from "fibers/future";
import fs from "fs";
[
  "insert",
  "update",
  "remove"
].forEach((fnName) => {
  const oldFn = Meteor.Collection.prototype[fnName];
  Meteor.Collection.prototype[fnName] = function(...args) {
    let fiber = Fiber.current;
    if (fiber && fiber.mongo) {
      fiber.mongo.total[fnName] ++;
    }
    return oldFn.call(this, ...args);
  };
});



let currentCPUUsage = 0;
const RECOMPUTE_SECONDS = 3;

export function configure(config) {

}

function time() {
  return new Date().getTime();
  let time = process.hrtime();
  return time[0] + Math.round(time[1] / 1000000);
}
function getStats() {
  //const data = fs.readFileSync("/proc/" + process.pid + "/stat");
  //return { ts: new Date().getTime(), data: data.toString().split(" ")};

  const data = process.cpuUsage();
  return { ts: time(), data: data.user + data.system};
}

let lastRes2;
function getUsage() {
  let res1 = lastRes2 || getStats();
  let res2 = getStats()
  let startTicks = res1.data;//parseInt(res1.data[13]) + parseInt(res1.data[14]);
  let endTicks = res2.data;//parseInt(res2.data[13]) + parseInt(res2.data[14]);
  let delta = endTicks - startTicks;
  let deltaTimeMS = res2.ts - res1.ts;
  lastRes2 = res2;
  if (deltaTimeMS === 0) {
    return currentCPUUsage;
  }
  let percent = 100 * (delta / 1000000) * (1 / deltaTimeMS * 1000);
  return percent;
}

function yieldForMongoUpdates(fiber) {

}

function yieldForSelfCpu(fiber, cpuLimit) {
  let running = fiber.runningTicks;
  let currentTicks = process.cpuUsage();
  let total = (currentTicks.user + currentTicks.system);
  running += (total - fiber.lastTicks);
  const per = 100 / (total - fiber.startTicks) * running;
  if (per > cpuLimit) {
    fiber.runningTicks = running;
    fiber.lastTicks = total;
    fiber.dontCalculateTicks = true;
  }
  return per > cpuLimit ? per : false;
}

function yieldForGlobalCpu(cpuLimit) {
  let cpu = currentCPUUsage;
  if (!lastRes2 || (time() - lastRes2.ts) > RECOMPUTE_SECONDS * 1000) {
    cpu = currentCPUUsage = getUsage();
  }
  return cpu > cpuLimit;
}

let saved = [];
export const LimitedTaskRunner = {
  fibers: {},
  lastId: 0,
  run(boundFn, options = {}) {
    const future = new Future();
    let currentId = ++LimitedTaskRunner.lastId;
    let boundValues = Fiber.current._meteor_dynamics ? Fiber.current._meteor_dynamics.slice(0) : [];
    const fiber = Fiber(() => {
      let savedValues = Fiber.current._meteor_dynamics;
      Fiber.current._meteor_dynamics = boundValues.slice();
      try {
        future.return(boundFn(fiber));
      }
      catch (e) {
        future.throw(e);
      }
      finally {
        Fiber.current._meteor_dynamics = savedValues;
        delete LimitedTaskRunner.fibers[fiber._id];
      }
    });
    fiber.maybeYield = maybeYield;

    process.nextTick(() => {
      fiber._id = currentId;
      let current = process.cpuUsage();
      fiber.yields = 0;
      fiber.options = options;
      fiber.startTicks = current.user + current.system;
      fiber.lastTicks = fiber.startTicks;
      fiber.runningTicks = 0;
      fiber.mongo = {
        total: {
          update: 0,
          insert: 0,
          remove: 0
        }
      };
      fiber.lastCalc = time();
      fiber.run();
    });
    return future;
  },
  maybeYield() {
    const current = Fiber.current;
    if (current && current.maybeYield) {
      return current.maybeYield();
    }
  },
  yieldIf(fn) {
    let shouldYield = fn({
      currentCPUUsage
    });
    if (shouldYield) {
      Fiber.yield();
    }
  }
}
LimitedTaskRunner.fibers = new WeakMap();

let running = false;
Meteor.setInterval(() => {
  if (running) {
    return;
  }
  running = true;
  if (!lastRes2 || lastRes2.ts < new Date().getTime() - 3000) {
    const usage = getUsage();
    currentCPUUsage = usage;
  }
  //console.log(currentCPUUsage);
  running = false;
}, 3000);

Meteor.setInterval(() => {
  Object.keys(LimitedTaskRunner.fibers).forEach((k) => {
    let fiber = LimitedTaskRunner.fibers[k];
    let resume = true;
    if (fiber) {
      if (fiber.options && fiber.options.resume) {
        let cpu = currentCPUUsage;
        if (!lastRes2 || lastRes2.ts < new Date().getTime() - 3000) {
          cpu = getUsage();
        }
        resume = cpu < fiber.options.resume;
      }
      if (resume) {
        process.nextTick(() => {
          let current = process.cpuUsage();
          fiber.lastTicks = current.user + current.system;
          delete LimitedTaskRunner[k];
          fiber.run();
        });
      }
    }
  });
}, 100);


function maybeYield() {
  const current = this;
  const yieldConfig = current && current.options && current.options.yield;
  if (!yieldConfig) {
    return;
  }
  let shouldYield = false;
  let skipCalc = false;
  if (!shouldYield && yieldConfig.cpu !== undefined) {
    shouldYield = yieldForGlobalCpu(yieldConfig.cpu);
  }
  if (!shouldYield && yieldConfig.selfCpu !== undefined) {
    shouldYield = yieldForSelfCpu(current, yieldConfig.selfCpu);
    skipCalc = shouldYield;
  }
  if (shouldYield) {
    if (!skipCalc) {
    }
    LimitedTaskRunner.fibers[current._id] = current;
    Fiber.yield();
  }
}


const origYield = Fiber.yield;

Fiber.yield = function() {
  let current = Fiber.current;
  return origYield.call(Fiber);
  if (current && current.startTicks) {
    if (!current.dontCalculateTicks) {
      let currentTicks = process.cpuUsage();
      let total = (currentTicks.user + currentTicks.system);
      current.runningTicks += (total - current.lastTicks);
      current.lastCalc = time();
    }
    current.dontCalculateTicks = false;
  }
};
