// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by limit-resources.js.
import { name as packageName } from "meteor/znewsham:limit-resources";

// Write your tests here!
// Here is an example.
Tinytest.add('limit-resources - example', function (test) {
  test.equal(packageName, "limit-resources");
});
